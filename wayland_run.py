#!/usr/bin/env python3
# Copyright 2019 Christoph Reiter
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
from typing import Tuple
import os
import subprocess
import tempfile
import time
import shutil
import argparse
import signal


def wait_for_lock(runtime_dir: str, timeout: float = 10) -> str:
    wait = 0.001
    while timeout > 0:
        entries = os.listdir(runtime_dir)
        for e in entries:
            if e.startswith("wayland-"):
                return os.path.join(runtime_dir, os.path.splitext(e)[0])
        timeout -= wait
        time.sleep(wait)
        wait *= 1.5
    else:
        raise SystemExit("timeout: mutter didn't start")


def get_mutter_version() -> Tuple[int, ...]:
    try:
        output = subprocess.check_output(["mutter", "--version"], universal_newlines=True)
    except FileNotFoundError:
        raise SystemExit("mutter not installed")
    version = output.splitlines()[0].rsplit(None, 1)[-1]
    return tuple(int(p) for p in version.split("."))


def check_xvfb() -> None:
    try:
        subprocess.check_output(["xvfb-run", "--help"], stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        pass
    except FileNotFoundError:
        raise SystemExit("xvfb-run not installed")


def main() -> object:
    parser = argparse.ArgumentParser(description='Run a command under wayland')
    parser.add_argument('COMMAND', type=str, nargs=argparse.REMAINDER, help='The command to run')
    args = parser.parse_args()
    command = args.COMMAND
    if not command:
        parser.print_help()
        return 1

    check_xvfb()
    mutter_version = get_mutter_version()

    temp_dir = tempfile.mkdtemp()
    try:
        wrapper_env = os.environ.copy()
        wrapper_env["XDG_RUNTIME_DIR"] = temp_dir
        wrapper_env.pop("DISPLAY", None)

        xvfb_args = ["xvfb-run", "-a",  "-s", "-screen 0 1280x1024x24", "--"]
        mutter_args = ["mutter", "--nested", "--wayland"]
        if mutter_version >= (1, 32):
            mutter_args += ["--no-x11"]

        with subprocess.Popen(xvfb_args + mutter_args, env=wrapper_env, start_new_session=True) as p:
            try:
                sub_env = os.environ.copy()
                sub_env["WAYLAND_DISPLAY"] = wait_for_lock(temp_dir)
                sub_env.pop("DISPLAY", None)
                return subprocess.run(command, env=sub_env).returncode
            except FileNotFoundError as e:
                return str(e)
            finally:
                os.killpg(p.pid, signal.SIGKILL)
                p.communicate(timeout=5.0)
    except KeyboardInterrupt:
        pass
    finally:
        shutil.rmtree(temp_dir)

    return None


if __name__ == '__main__':
    sys.exit(main())
