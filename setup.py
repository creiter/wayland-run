#!/usr/bin/env python3
# Copyright 2019 Christoph Reiter
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# type: ignore

from setuptools import setup


if __name__ == "__main__":

    with open('README.rst') as h:
        long_description = h.read()

    setup(
        name="senf",
        version="0.0.1",
        url="https://gitlab.gnome.org/creiter/wayland-run",
        description=("Run a command under wayland"),
        long_description=long_description,
        author="Christoph Reiter",
        author_email="reiter.christoph@gmail.com",
        python_requires='>=3.5, <4',
        py_modules=['wayland_run'],
        entry_points={
            "console_scripts": [
                "wayland-run = wayland_run:main",
            ]
        },
    )
