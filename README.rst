Wayland CI Wrapper
------------------

Install:
    ``pip3 install --user git+https://gitlab.gnome.org/creiter/wayland-run``

Use (make sure ``~/.local/bin`` is in PATH):
    ``wayland-run <my-command> <my-command-args>``